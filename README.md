Patch Notes
===========
# Multi-Gen Game
## Version 0.0.4
### Documentation 0.0.3-c

Updates this time include, but are not limited to:

-   Unit Spawning. New Units now spawn in **player-owned provinces**. Names are obtained pseudorandomly from a unit-name conf file.

-   Sorting. (This is a truly great development!) The developer is pleased to announce that you can *sort* the list of Units in the Army. Please do experiment with this. You should be able to sort by Name, Age, Level, Location, and Chosen/Not Chosen, by clicking on menu headers.

-   Solved graphical glitches that would occur during victory and defeat screens. (If you didn’t notice, don’t ask. Some of these were embarrassing.)

-   A dark Halloween theme. Special thanks to Zunethia for providing artwork.
=======
This branch facilitates the lengthy process of porting the MGG to SDL2.
Special thanks to a new contributor, mk, for providing the logo in res/sgdt\_logo1.png.

~~Documentation in the proceeding sections may be incorrect or incomplete!~~

~~As of Dec. 16th, 2018, now that the SDL game-loop code compiles, the remaining phases are:~~

1.  ~~resolve logical errors (especially regarding text rendering locations);~~

2.  ~~update the code with version 0.0.3 functionality, like Unit spawning, sorting, etc.;~~

3.  ~~refactor the code base into something more organized (any help would be greatly appreciated).~~

As of May 18, 2019, many of goals have been met (huge thanks to TrebledJ for help with Objective #3).

So, as far as I am concerned, the only changes that remain before we merge the branch are to:

4.  reduce memory leaks (mostly finished, but consider SDL\_Surfaces and Fonts);

5.  refactor further (for better modern C++ coding conventions, etc.);

6.  update aesthetic / art changes.

I believe that we can focus on adding new functionality *after* merging the branch, but I am happy to hear other thoughts / opinions on this.

EDIT: July 2019.

Well, I guess I added new functionality anyway. Version 0.0.4 will have walking animations at the TMB layer, which the user can toggle on/off using the 'a' key.
>>>>>>> remotes/origin/PortingToSDL2

Terms & Definitions
===================

1.  MGG - Multi-Gen Game. An elaborate, customizable, single-player, 2D grand strategy game, currently written in C++ using the Allegro 5 game libraries. Remark: this is a *working title*. The origin of the name will be explained in detail in a future edition. For now, it suffices to say that the game will take place in a fictional world over a period of centuries (see IGT), i.e. multiple generations.

2.  IGT - In-Game Time. This refers to the passage of time in the *in-game* calendar system. In the game, there are 18 months per year and 10 days per month. (This is subject to change in future versions and can be easily adjusted by changing the <span>DAYS\_PER\_MONTH</span> and <span>MONTHS\_PER\_YEAR</span> values in <span>src/game-loop.hpp</span>). Dates are represented (e.g. in the upper-right corner of the WMSL) as <span>yyy.mm.dd</span>.

3.  WMSL - World-Map Strategy Layer. This is the “outer layer” of the game, the one that is shown any time a game is launched. It is a system in which the user can make strategic decisions about unit placement, respond to Events, and pause and unpause the game (using the space bar). While unpaused, the game “moves forward in time” (see IGT) at a certain rate, modeling the passage of time in a fictional universe.

4.  TMB - Tactical-Map Battle. This is a turn-based mini-game that is periodically launched by the WMSL at deterministic moments, or pseudo-randomly depending on context.

5.  Unit - a character in the game. A Unit will represent a member of the Army.

6.  UC - Unit Class. This refers to the *occupation* or *unit type* for a particular Unit. At the moment, the only examples are Archer and Soldier. This will be greatly expanded later.

7.  Lead Developer - I’ve been using this term, and you may be wondering who it refers to. I often write documentation & emails in the third person (or occasionally the royal We), to sound more official. To be clear, I (Vincent) am the lead developer.

The WMSL
========

When the WMSL is unpaused, IGT will “tick” by, at an adjustable rate. To adjust this rate, try pressing the ‘+’ or ‘-’ keys when the game is *unpaused*. The number displayed in the upper-right corner approximately equals
10 \* (*p**a**c**e* *i**n* *t**i**c**k**s*/*s**e**c*).
 The maximum pace is 10 ticks per second, making 100 the maximum number displayed.[1]

The World Map is composed of **Provinces**, each of which contains **Settlements**. While the game is still active/running, some Provinces are player-owned, others are enemy-owned, and some may be contested (have player units and enemy units both present, both trying to conquer).There are three ways for the game to end:

-   The player conquers all provinces (prompting a Victory screen with a Score value).

-   All player units are debilitated (prompting a Defeat screen).

-   The player deliberately exits the game (“Cold exit”). A future release will auto-save the game state, but this has not been implemented.

**The WMSL also has music enabled by default. To pause or unpause the music, press the ‘m’ key**.

The WMSL, the foundation of the game is, like many other grand-strategy games, composed of different tabs.

‘Map’ Tab
---------

When this tab is selected, the world map is displayed. The user may click on a province to get data about that province (name, settlements, etc.). Once you have units in the Field of a Province, you may click the ‘S‘ button to invade a Settlement. (A new window is generated where you can use arrow keys to select the Settlement.) Unpause, give it a few moments, and a TMB should spawn.

‘Army’ Tab
----------

All player units are displayed in a scrollable list under the Army tab. This is the heart of the game, where the user can choose to move units into other provinces.

When a new game initiates, the unit list is read from a file, namely ‘res/world-map-2.conf’. You may adjust this file by adding or deleting lines that contain unit names.

When the game is unpaused, clicking a unit will simply display data for that unit. However, when it is paused, the user may left-click to see data OR right-click in order to add the selected unit into a temporary “group” along with other selected units. Then, when the group has been set up, the user may click the <span>P</span> symbol in the lower-right corner in order to set a destination Province for that group. (When paused, the user can move units to different provinces instantaneously.) Units will land in the “Field” of the Province. After that, you can issue the order to *invade a Settlement*; this order is issued under the Map tab.

‘Research’, ‘Homefront’ Tabs
----------------------------

(Requirements & design pending)

The TMB
=======

Terrain Tile Codes
------------------

Each TMB takes place on a map. Map data is read from files, each of which contains a space-delimited table of integers. Each integer (corresponding to a tile in the map) represents the **terrain type** of that tile. A key for these terrain type codes should be inside ‘src/oo-tmb.cpp’ and is reproduced below:

| **Terrain Code** | **Interpretation** |
|:----------------:|:------------------:|
|         0        |      Ocean[2]      |
|         1        |       Plains       |
|         2        |       Forest       |
|         3        |        Hills       |
|         4        |         Ice        |
|         5        |        Sand        |
|         6        |      Mountains     |

Mechanics
---------

All TMBs are turn-based. Player units have their HP displayed in GREEN font whereas the enemy AI unit HP is in RED[3]. The user may click the icon/sprite for their units, at which moment the right-hand menu updates and gives you options for giving that unit instructions. Go through these options carefully. When you’ve finished giving orders to each of your units, the turn phase updates (i.e., the enemy takes its turn, issuing orders to its units). Then it is the player turn again and the cycle continues.

Types of TMBs
-------------

There are different variants of TMBs, each with their own victory and defeat conditions (feel free to study them yourself). The most common (and the only one currently used in Alpha Testing) is the RoutTMB, in which the only way to win is to leave all opponent units debilitated.

Omega Mode
----------

Some open-source games have an Omega Mode that allows the player to have invincibility or special powers. This game offers a slightly different cheat: allowing the AI to play through a TMB *in place of* the user. This can be activated by pressing three keys in sequence: <span>g</span>, <span>o</span>, <span>d</span>. It can be *deactivated* by pressing the <span>m</span> key.

Known Bugs
==========

**Error while loading shared libraries: libwebp.so.7: no such file or directory** - If you get this error (or a similar one involving a different shared library file), there are multiple solutions. You may be able to copy a shared library from the ‘for-static-release-dynamic-libs’ directory into your system directory, using a command like: <span>cp -i for-static-release-dynamic-libs/(libname) /usr/lib</span>. Alternatively, you could try simply running the game with <span>LD\_LIBRARY\_PATH=./for-static-release-dynamic-libs ./bin/static/debug/game-loop</span>. Should both of these approaches fail, then you need to install a new library, one that is not included in this game package. If you need help, contact the Lead Developer.

**After invading a province, I get a black screen** - believe it or not, this one is not a bug; it’s a feature. (<span>:P</span>) But in all seriousness, when this happens, you’re looking at a *new* black window. Try clicking back to the original window and un-pausing in order to *jar* the system back into motion.

**After invading a province, I get a grey/empty/non-responsive screen** - believe it or not, this one is not a bug; it’s a feature. (:D) But in all seriousness, when this happens, you’re looking at a *new* window. The game expects you to make a choice, and after you choose to Fight, the TMB is added to the list of pending battles. Try clicking back to the original window and un-pausing in order to *jar* the system back into motion and load the battles.
**Upon pressing the ‘P’ button to invade a Field, the game crashes** - this is a serious error that only seems to happen on VMs. To minimize the risk of this error, try un-pausing the game for a few seconds *before* you pause and select your army. If this still doesn’t work, then try scrolling up and down through the unit list before selecting (right-clicking) unit members. If you want to report this error, let the developer know and provide as much information as possible (OS and version, system specs, call-stack when running through GDB, etc.). In the unfortunate event that you cannot ever avoid this error, you may still compile the source and call <span>./bin/static/release/oo-tmb 0</span> to test a basic TMB. (Even if the WMSL doesn’t work, the TMB should be much more stable.)

Help Wanted
===========

The project could use help in a variety of areas; this list is sorted by most to least important work:

The project could use help in a variety of areas, e.g.:

-   Artwork (unit sprites, maps, logo, etc.) and/or music;

-   More maps for Tactical Map Battles;

-   Support for other operating systems;

-   Play testing and bug filing / fixing;

-   Clearer or more complete documentation;

-   Code refactoring & optimization;

-   More names for Units[4].

Bounties
========

Active bounties (for more serious endeavors) will be listed here. Note the following:

1.  If you are the first to complete one of the tasks, you are eligible to receive the bounty *and* to have your (user)name listed as a Major Contributor to the project.

2.  Bounty prices will start low (maybe even at zero!) and rise over time.

3.  Bounties will close the moment someone redeems them. If no one redeems them, their status will be renewed or closed on the next patch update.

| **Code** | **Award** | **Description**                                                                     |
|:--------:|:---------:|:------------------------------------------------------------------------------------|
|  B00000  |   $0.00   | Automatic edge detection & adjacent province identification.                        |
|          |           | In the current framework, the “adjacent Province” system must be                    |
|          |           | hard-coded for each World Map. If a program were developed to                       |
|          |           | process/analyze the world-map image and find adjacent provinces                     |
|          |           | automatically, this would be most impressive and useful. If you can solve           |
|          |           | this problem, please contact the Lead Developer.                                    |
|  B00001  |   $0.00   | The game compiles on GNU/Linux systems. However, the same exact                     |
|          |           | code does not compile on Windows using MinGW. To redeem the bounty:                 |
|          |           | write a revision to the code (probably using <span>\#ifdef</span>s) that will allow |
|          |           | the same code to compile on both systems.                                           |
|  B00002  |   $0.00   | Package the MGG into a .deb file, using proper Linux path conventions.              |
|          |           | (This is harder than it sounds. If you succeed, show the Lead                       |
|          |           | Developer how you did it.)                                                          |
|  B00003  |   $0.00   | Release a version for Android. (Again, if you succeed, show the Lead                |
|          |           | Developer. The game needs to be at least as stable as the desktop version.)         |

Details on How to Test
======================

GNU/Linux
---------

In order to *compile* the game, you will need the Allegro 5 libraries installed on your computer (in order for the linking step to succeed). See the following resources for details:

-   <https://wiki.allegro.cc/index.php?title=Getting_Started>

-   <https://wiki.allegro.cc/index.php?title=Install_Allegro5_From_Git/Linux/Debian>

A debian-based installation script (inspired by the above resources) has been attached to this release. Note that it builds dynamic libraries for Allegro, so should you later compile the MGG on your own machine, the final executables will be located somewhere under <span>bin/dynamic/</span>.

However, on Linux-based platforms, it is *not* strictly **necessary** to compile the MGG itself in order to test it. There is a run-script (conveniently called <span>run-script.sh</span>) that will try running the game executables in at least four different ways. Note, however, that if the game crashes, this script will simply try to run it again. Thus, *when using this script*, if the game spontaneously reloads and shows a title screen, that means that there was actually a crash - and it may be difficult to track down the error after the fact.

Credits
=======

Special thanks to all contributors. These include, in alphabetical order by username:

*    MK (SGDT logo, additional artwork - 2018).
*    MrMcApple (co-development of original Java demo - 2017),
*    Slpee (concept, original design/vision),
*    TrebledJ (co-development of C++ code, 2019 onward)
*    Zunethia (primary testing, artwork)

Music credit:

Lobo Loco, “Snowmelt Yukkon River”

Creative Commons by Attribution

[1] You may wonder what the minimum pace is. The remarkable fact is: there *is* none - you should be able to set the speed arbitrarily slow, though we’re not sure why you’d want to.

[2] currently, no Units can directly cross bodies of water. There may be flying units in a future release.

[3] You may wonder, *how do I know which units belong to which side*? Your units have GREEN HP text and the enemy units have red HP text. The developer considers making this an easily-adjustable color scheme in the future.

[4] Many names have already been generated. Simply note that trusted contributors have the option to have a player Unit named after them in the final release.
