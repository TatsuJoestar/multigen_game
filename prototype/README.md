## Prototypes

This directory will contain test implementations (proofs of concept) for various components of the game. So far, it contains an implementation of an undirected graph and a breadth-first search. This will prove useful in representing the adjacency graph of Provinces on the world map.


