#pragma once
/* A debugging namespace, as written by Technist. */
namespace Debug
{
  /* Usage:
   debug("Starting...", 3, 2, 1);
   if (SDL_Init(SDL_INIT_VIDEO) < 0)
   {
     debug("SDL could not initialise!", debugSDLError());
     //  ...
   }
   */
  
  std::ostream &DebugStream = std::cout; 
  
  template<typename Type>
  std::ostream& operator<< (std::ostream &os, const std::vector<Type> &container)
  {
    for (auto it = container.begin(); it != container.end(); ++it)
    {
      os << " " << *it << std::endl;
    }
    return os;
  }
  
  void debug()
  {
    DebugStream << std::endl;
    DebugStream.flush();
  }
  
  template<class T>
  void debug(T arg)
  {
    DebugStream << arg << std::endl;
    DebugStream.flush();
  }
  
  template<class T, class... Ts>
  void debug(T arg, Ts... args)
  {
    DebugStream << arg << " ";
    debug(args...);
  }
  
  std::string debugSDLError()
  {
    std::string ret = "SDL_ERROR: ";
    ret += SDL_GetError();
    
    return ret;
  }
  
}  //  namespace Debug
/*using namespace Debug;*/
